30 aug (wo) 


Onze groepsnaam, -logo en -icoon bedacht.


 31 aug (do) 
Hoorcollege gekregen en onze opdracht ontvangen.


 4 september (ma) 
We hebben een team captain uitgekozen --> Puck
Samen met het team de regels vaststellen, teamposters gemaakt, taakverdeling/planning gemaakt.
studiecoaching gekregen
 5 sept (di) 
Hoorcollege Design theorie
Verder met onderzoeken en spullen voor woensdag voor het moodboard verzamelen. (alle verzamelde afbeeldingen zijn zwart-wit uitgeprint dus het moest digitaal).
 6 sept (wo) 
Moodboard digitaal afgemaakt.
Workshop blog maken
 7 sept (do) 
Werk college design challange gehad
 8 sept (vr) 
Aantekeningen digitaal overgenomen
Nog onderzoek/onderzoeksvragen/moodboards erin zetten
 11 sept (ma) 
Met het team één spel van de vijf individuele bedachte spellen uitgekozen.
We hebben nog van de overige spellen bepaalde stukjes erbij gevoegd.
Taken verdeeld
 12 sept (di) 
Hoorcollege design theory: verbeelden
 13 sept (wo) 
Workshop moodboard gehad
Verder aan ons spel
Studie coaching gehad
 14 sept (do) 
Keuzevak illustrator gevolgd
Feedback gevraagd --> feedback gekregen
Spelanalyse gemaakt
 18 sept (vr) 
Paperprototype presenteren
Briefing 2 ontvangen --> kill your darling
Nieuwe planning maken
Inschrijven studiekeuzevak
 19 sept (di) 
Hoorcollege prototyping
 20 sept (wo) 
Spel analyse van 3 bestaande spellen; tafelvoetbal, saboteurs, fifa 
Onderzoeken van spelregels
 21 sept (do) 
Illustrator gevolgd
Werkcollege gekregen
 25 sept (ma) 
3 concepten met elkaar bedacht: escaperoom, geochache, lasergamen
Feedback gekregen van Bob, geprobeerd om de 3 concepten samen te brengen.
26 sept (di)
Hoorcollege onderzoeken gehad
27 sept (wo)
Verder met het uitwerken van ons project; beginnen aan paperprototype escaperoom
28 sept (do)
Illustrator gehad, verder met paperprototype maken: escaperoom afgemaakt, verder aan map van Rotterdam en schermen voor het online gedeelte van het spel.
Werkcollege gehad.
29 sept (vr)
0-meting handvaardigheid.
1 okt (zo)
Met team besproken over hoe het gaat met het teamwerk/inzet, naar aanleiding van Puck haar mail.
2 okt (ma)
Laatste dingen van het prototype afmaken
Presentatie stukken verdelen: ik vertel het spelverloop, dus tekst over spelverloop maken.
3 okt (di)
Geen hoorcollege, tekst van presentatie doornemen.
4 okt (wo)
Presentatie dag, voor alumni presenteren. Feedback gekregen.
5 okt (do)
Illustrator gehad
0 meting Nederlands en Engels
9 okt (ma)
Nieuwe iteratie, veranderingen aan het spel maken: rekening houden met de feedback van woensdag.
10 okt (di)
Geen hoorcollege, leren voor tentamen design theory. Minigames verzinnen.
Details verzinnen voor in de escaperoom.
11 okt (wo)
Verder aan spel. Workshop leesdossier. Verder aan de minigames met Serhat en Puck.
12 okt (do)
Illustrator gevolgd. Tentamen gemaakt.
16 okt (ma)
Met team afgesproken om verder aan het spel te werken.
22 okt (zo)
Blog bijgewerkt, minigames uitgewerkt, details voor (vernieuwde) escaperoom uitgewerkt.
23 okt (ma)
Nog meer details voor de escaperoom maken, door voor de escaperoom verven. Ontwerpproceskaart gemaakt. Overzicht gemaakt van wat nog af moet  niet alles af gekregen, morgen verder.
24 okt (di)
Minigames getest, escaperoom afgemaakt, logo + naam bedacht (en ontworpen), spelanalyse gemaakt, spelregels nagekeken en aangekruist op de map waar de minigames zich bevinden. Oneliner voor expo bedacht.
25 okt (wo)
Expositie gehad. Feedback gekregen. Peerfeedback ingevuld en zelf teruggekregen. Studie coaching gehad over het leerdossier.
Blog bijwerken, stars schrijven.
